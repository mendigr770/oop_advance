﻿using System;
using System.Collections.Generic;

namespace comprtotion
{
    class Singer : Person, Contender, Player
    {
        private int rate;
        private Song mySong;
        public string Name() => base.name;

        public int Rate() => this.rate;
        public Song MySong() => this.mySong;

        public void setRate(int value)
        {
            this.rate += value;
        }

        public string talent() => $"Im a {this.GetType()}";

        public Singer(string name = "") : base(name) { }
        public Singer() : base("") { }
        public Singer(Singer oldSinger) : base(oldSinger.name)
        {
            this.rate = oldSinger.Rate();
        }
        public void playSong()
        {
            Console.WriteLine($"my name is: {name}");
            Console.WriteLine($"my song is: {mySong.Name}");
        }

        public static Contender findMaxRate(List<Contender> singers)
        {
            Random random = new Random();
            Contender maxRateSinger = singers[random.Next(singers.Count)];  
            foreach (Contender singer in singers)
            {
                if (singer.Rate() > maxRateSinger.Rate())
                {
                    maxRateSinger = singer;
                }
            }
            return maxRateSinger;
        }

        public void choseSong(Song[] songs)
        {
            Random random = new Random();
            this.mySong = songs[random.Next(songs.Length)];
        }

    }
}
