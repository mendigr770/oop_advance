﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace comprtotion
{
    class Guitarist : Person, Player
    {
        public Guitarist(string name) : base(name) { }
        public Guitarist() : base("") { }

        public string talent() => $"{this.GetType()}";
    }
}
