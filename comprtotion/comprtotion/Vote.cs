﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace comprtotion
{
    abstract class Vote
    {
        public static int voteToSingerNumber(List<Contender> singers)
        {
            Random random = new Random();
            return random.Next(singers.Count);
        }

        public static Contender findMaxRate(List<Contender> contenders)
        {
            Random random = new Random();
            Contender maxRateCont = contenders[random.Next(contenders.Count)];
            foreach (Contender contender in contenders)
            {
                if (contender.Rate() > maxRateCont.Rate())
                {
                    maxRateCont = contender;
                }
            }
            return maxRateCont;
        }
    }
}
