﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace comprtotion
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Player> players = new List<Player>()
            {
                new Singer(),
                new Drumer(),
                new Guitarist()
            };
            Judge simon = new Judge("simon");
            List<Contender> competitors = new List<Contender>();
            competitors.Add(new Band(players, "The band"));
            competitors.Add(new Singer("Christina"));
            competitors.Add(new Singer("Lady"));
            competitors.Add(new Singer("Madona"));
            competitors.Add(new Singer("Shakira"));
            competitors.Add(new Singer("Ke$ha"));
            competitors.Add(new Singer("Adele"));
            competitors.Add(new Singer("Britney"));
            competitors.Add(new Singer("Niqi"));
            competitors.Add(new Singer("Kelly"));
            competitors.Add(new Singer("Kety"));
            competitors.Add(new Singer("Justin"));
            competitors.Add(new Singer("Avril"));
            competitors.Add(new Singer("Rihanna"));
            competitors.Add(new Singer("Beyoncé"));
            Console.WriteLine(competitors[0]);
            Song[] songs = new Song[3] { new Song("Song1"), new Song("Song2"), new Song("Song3") };
            List<Voter> voters = new List<Voter>() { };

            //level 2
            List<Contender> continuesSingers = new List<Contender>(competitors);
            while (continuesSingers.Count > 1)
            {
                continuesSingers = Competition.copmetotion(continuesSingers, simon);
            }
            Console.WriteLine($"the winner is {continuesSingers[0].ToString()}");

            // Level 3 
            Competition.VotersPower(ref competitors, voters);
            foreach (Contender singer in competitors)
            {
                singer.choseSong(songs);
            }
            Contender winner = Singer.findMaxRate(competitors);
            Console.WriteLine($"the winner is: {winner} " +
                              $"with rating of {winner.Rate()} " +
                              $"with song {winner.MySong().Name}");
            Console.WriteLine("debug");
            Console.Read();
        }
    }
}
