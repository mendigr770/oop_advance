﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace comprtotion
{
    class Band : Contender
    {
        private string name;
        private int rate;
        private Song mySong;
        private List<Player> players;
        //private List<Type> TypsArray;
        public string Name() => this.name;

        public int Rate() => this.rate;

        public void setRate(int value)
        {
            this.rate += value;
        }

        public Song MySong() => this.mySong;

        public Band(List<Player> players, string name = "")
        {
            if (!ValidateBandPropriety(players))
            {
                throw new ArgumentException("validate your Band proprity");
            }

            this.name = name;
            this.players = players;
        }

        private static bool ValidateBandPropriety(List<Player> players)
        {
            return players.OfType<Singer>().Any() &&
                   players.OfType<Drumer>().Any() &&
                   players.OfType<Guitarist>().Any();
        }

        public Band(Band oldBand)
        {
            this.rate = oldBand.Rate();
            this.name = oldBand.name;
        }
        public void playSong()
        {
            Console.WriteLine($"my name is: {name}");
            Console.WriteLine($"our song is: {mySong.Name}");
        }

        public void choseSong(Song[] songs)
        {
            Random random = new Random();
            this.mySong = songs[random.Next(songs.Length)];
        }

        public override string ToString() => this.name == null ? "this.name" : "no name";

    }
}
